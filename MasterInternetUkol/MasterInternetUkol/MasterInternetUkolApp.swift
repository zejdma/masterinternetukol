//
//  MasterInternetUkolApp.swift
//  MasterInternetUkol
//
//  Created by Martin Zejda on 19.02.2022.
//

import SwiftUI

@available(iOS 14.0, *)
@main
struct MasterInternetUkolApp: App {
    var body: some Scene {
        WindowGroup {
                ContentView()
        }
    }
}
