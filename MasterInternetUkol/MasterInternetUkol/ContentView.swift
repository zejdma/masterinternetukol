//
//  ContentView.swift
//  MasterInternetUkol
//
//  Created by Martin Zejda on 19.02.2022.
//

import SwiftUI

@available(iOS 14.0, *)
struct ContentView: View {
    
    var body: some View {
        NavigationView {
            ZStack {
                Color("Background")
                .edgesIgnoringSafeArea(.all)
                
                Button(action: {
                 }) {
                     NavigationLink(destination: SettingsView()) {
                     Text("Settings")
                             .foregroundColor(.white)
                     }
                 }
            }
        }
        .accentColor(.white)
    }
}

@available(iOS 14.0, *)
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
