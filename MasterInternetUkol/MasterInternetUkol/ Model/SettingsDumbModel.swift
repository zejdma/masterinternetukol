//
//  SettingsDumbModel.swift
//  MasterInternetUkol
//
//  Created by Martin Zejda on 19.02.2022.
//

import Foundation

struct SettingsDumbModel: Codable, Hashable {
        let icon: String
        let title: String
        let text: String
        let link: String
}
