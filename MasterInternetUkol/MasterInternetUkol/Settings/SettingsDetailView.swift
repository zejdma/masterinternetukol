//
//  SettingsDetailView.swift
//  MasterInternetUkol
//
//  Created by Martin Zejda on 19.02.2022.
//

import SwiftUI

@available(iOS 14.0, *)
struct SettingsDetailView: View {
    
    let dumbSetting: SettingsDumbModel
    
       var body: some View {
           ZStack {
               Color("Background")
               .edgesIgnoringSafeArea(.all)
               
               VStack {
                   SettingsBodyView(title: dumbSetting.title, text: dumbSetting.text)
                   VideoTutorialView(link: dumbSetting.link)
               }
           }
           .edgesIgnoringSafeArea(.top)
       }
}

@available(iOS 14.0, *)
struct SettingsDetailView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsDetailView(dumbSetting: SettingsViewModel().settings[0])
    }
}


@available(iOS 13.0, *)
struct SettingsBodyView: View {
    let title: String
    let text: String
    
    var body: some View {
        VStack(alignment: .leading) {
            Image("MotionDetectionPlaceholder")
                .resizable()
                .aspectRatio(contentMode: .fit)
            Text(title)
                .font(.headline)
                .fontWeight(.semibold)
                .padding(.vertical)
            Text(text)
                .font(.callout)
                .fontWeight(.light)
        }
        .padding(.horizontal, 16)
        .padding(.vertical)
        .background(Color("DetailBackground"))
    }
}

@available(iOS 14.0.0, *)
struct VideoTutorialView: View {
 let link: String
    var body: some View {
        VStack{
            Text("Video tutorial")
                .font(.body)
                .padding(.top)
                .padding(.vertical,2)
            Text("Watch the video and learn how to use the app! It’s easy!")
                .font(.caption)
                .fontWeight(.light)
            Link(destination: URL(string: link)!) {
                Label {
                    Text("Play video")
                        .font(.body)
                        .foregroundColor(.black)
                } icon: {
                    Image("youtube")
                }
                .padding(.horizontal, 28)
                .padding(.vertical, 9)
                .background(
                    RoundedRectangle(cornerRadius: 100, style: .continuous).fill(Color.white)
                )
                .padding(10)
            }
        }
    }
}

