//
//  Settings.swift
//  MasterInternetUkol
//
//  Created by Martin Zejda on 19.02.2022.
//

import SwiftUI

@available(iOS 14.0, *)
struct SettingsView: View {
    @StateObject private var viewModel = SettingsViewModel()
    
    init() {
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white, .font : UIFont(name:"SFPro-semibold", size: 20)!]
        UITableView.appearance().backgroundColor = UIColor.clear
        UITableView.appearance().separatorColor = UIColor(Color("Divider"))
    }

    var body: some View {
        ZStack {
            Color("Background")
            .edgesIgnoringSafeArea(.all)
            
            List {
                Section() {
                    ForEach(viewModel.settings, id:\.self) { dumbSetting in
                        NavigationLink(destination: SettingsDetailView(dumbSetting: dumbSetting)) {
                            VStack(alignment: .leading){
                                ListItem(image: dumbSetting.icon, text: dumbSetting.title)
                                    .padding(.vertical)
                            }
                        }
                    }
                    .listRowBackground(Color.white)
                    .foregroundColor(.black)
                    
                }
                Section() {
                    ForEach(viewModel.settings1, id:\.self) { dumbSetting in
                        NavigationLink(destination: SettingsDetailView(dumbSetting: dumbSetting)) {
                            VStack(alignment: .leading){
                                ListItem(image: dumbSetting.icon, text: dumbSetting.title)
                                    .padding(.vertical)
                            }
                        }
                    }
                }
                Section() {
                    ForEach(viewModel.settings2, id:\.self) { dumbSetting in
                        NavigationLink(destination: SettingsDetailView(dumbSetting: dumbSetting)) {
                            VStack(alignment: .leading){
                                ListItem(image: dumbSetting.icon, text: dumbSetting.title)
                                    .padding(.vertical)
                            }
                        }
                    }
                }
            }
            .navigationBarTitle("How use this app", displayMode: .inline)
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

@available(iOS 14.0, *)
struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}

@available(iOS 13.0, *)
struct ListItem: View {
    var image: String
    var text: String
    
    var body: some View {
        HStack() {
            Image(image)
            Text(text)
        }
        .multilineTextAlignment(.leading)
    }
}


